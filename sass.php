<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Kleyston</title>

  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="css/style.css">    

  <!-- CSS Filtro Portfólio -->
  <link rel="stylesheet" href="css/filtro.css">

 <!-- CSS Materialize -->
  <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/materialize.css">   
   <link rel="stylesheet" type="text/css" href="css/gradient-materialize.css">  
  <!-- Icon -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


</head>
<body>

<!-- Página Curtir Facebook  -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11&appId=1634733299937003&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>




  <div id="scroll-home">    <!-- Scroll Home -->

    <div class="navbar-fixed">
     <!-- Dropdown Structure -->
     <ul id='dropdown1' class='dropdown-content'>
      <li><a href="#!" class="nav-drop">Blog</a></li>
      <li><a href="#!" class="nav-drop">Serviços</a></li>
      <li class="divider"></li>
      <li><a href="#!" class="nav-drop">Contato</a></li>
    </ul>

    <nav class="teal accent-4 navbar">
      <div class="nav-wrapper container">
             <!-- Logo -->
        <a href="#!" class="brand-logo"><i class="material-icons">person</i>Kleyston</a>
        <ul class="right hide-on-med-and-down">
          <li class="active"><a class="scroll" href="#scroll-home"><i class="material-icons large">home</i></a></li>
          <li><a class="scroll" href="#scroll-servico">Serviços</a></li>
          <li><a class="scroll" href="#scroll-portfolio">Portfólio</a></li> 
          <li><a class="scroll" href="#scroll-sobre">Sobre</a></li>
          <li><a class="scroll" href="#scroll-contato">Contato</a></li>
          <!-- Dropdown Trigger -->
          <li><a class='dropdown-button' data-beloworigin="true" href='#' data-activates='dropdown1'>Drop Me!</a></li>
        </ul>
      </div>
    </nav>

  </div>  <!-- Fecha Div Navbar-Fixed -->
</div>  <!-- Fecha Div Scroll Home -->

<!-- Fim NavMenu -->



<!--  === Parallax Principal === -->

<div class="parallax-container parallax-destaque">

  <div class="parallax"><img src="img/war.jpg"></div>

  <div class="card-content container center-align">
    <h4 class="white-text txt-parallax">I am a very simple card.</h4>
    <p style="color: white">I am a very simple card. I am good at containing small bits of information.<br/></p>
    <a class="waves-effect waves-light btn pulse teal accent-4 btn-parallax"><i class="material-icons right">cloud</i>button</a>
  </div>

</div>  <!-- Fecha Div Parallax Container -->

<!-- Fim Parallax Principal -->



<!-- ==== Serviços === -->

<div id="scroll-servico">   <!-- Scroll Page Servico -->

  <div class="section white  servico">
    <div class="row container">
      <h4 class="center-align">Serviços</h4>

      <div class="row">
        <div class="col s12 m4">
          <div class="card-panel z-depth-0">
            <div class="card-content center-align">
             <i class="material-icons large section icon-servico">computer</i>  <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
           </div>    
         </div>
       </div>

       <div class="col s12 m4">
        <div class="card-panel z-depth-0">
          <div class="card-content center-align">
           <i class="material-icons large section  icon-servico">face</i>  <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
         </div>    
       </div>
     </div>

     <div class="col s12 m4">
      <div class="card-panel z-depth-0">
        <div class="card-content center-align">
         <i class="material-icons large section  icon-servico">settings</i>  <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
       </div>    
     </div>
   </div>
 </div> 

</div>   <!-- Fecha Div Row -->
</div>   <!-- Fecha Div Section -->
</div>   <!-- Fecha Div Scroll -->



<!-- === Portfólio ===  -->

<div id="scroll-portfolio">   <!-- Scroll Page Portfólio -->

  <!-- Menu Filtro Portfólio -->
  <h4 class="header center section">Portfólio</h4>
  <div class="category_list container center">
    <a href="#scroll-portfolio" class="category_item" category="all">Todos</a>
    <a href="#scroll-portfolio" class="category_item" category="designegrafico">Design Gráfico</a>
    <a href="#scroll-portfolio" class="category_item" category="webdesign">Web Design</a>
    <a href="#scroll-portfolio" class="category_item" category="midiasocial">Mídia Social</a>
    <a href="#scroll-portfolio" class="category_item" category="logos">Logos</a>
  </div>

  <div class="row container">
    <br/>

    <!-- Portfólio 01 -->
    <div class="col s12 m6 l4">
      <div class="card hoverable  product-item" category="webdesign">
        <div class="card-image materialboxed" data-caption="A picture of some deer and tons of trees">
          <img src="img/port/rick.png">
          <span class="card-title">Design Gráfico</span>
          <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">favorite_border</i></a>
        </div>
        <div class="card-content">
          <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
        </div>
        <div class="card-action">
          <a href="#" class="portfolio-link">This is a link</a>
        </div>
      </div>
    </div>

    <!-- Portfólio 02 -->
    <div class="col s12 m6 l4">
      <div class="card hoverable product-item"  category="designegrafico">
        <div class="card-image  materialboxed" ">
          <img src="img/port/rick.png">
          <span class="card-title">Design Gráfico</span>
          <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">favorite_border</i></a>
        </div>
        <div class="card-content">
          <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
        </div>
        <div class="card-action">
          <a href="#" class="portfolio-link">This is a link</a>
        </div>
      </div>
    </div>

    <!-- Portfólio 03 -->
    <div class="col s12 m6 l4">
      <div class="card hoverable product-item"  category="designegrafico">
        <div class="card-image  materialboxed">
          <img src="img/port/rick.png">
          <span class="card-title">Design Gráfico</span>
          <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">favorite_border</i></a>
        </div>
        <div class="card-content">
          <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
        </div>
        <div class="card-action">
          <a href="#" class="portfolio-link">This is a link</a>
        </div>
      </div>
    </div>

    <!-- Portfólio 04 -->
    <div class="col s12 m6 l4">
      <div class="card hoverable product-item"  category="webdesign">
        <div class="card-image  materialboxed">
          <img src="img/port/rick.png">
          <span class="card-title">Web Design</span>
          <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">favorite_border</i></a>
        </div>
        <div class="card-content">
          <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
        </div>
        <div class="card-action">
          <a href="#" class="portfolio-link">This is a link</a>
        </div>
      </div>
    </div>

    <!-- Portfólio 05 -->
    <div class="col s12 m6 l4">
      <div class="card hoverable product-item"  category="webdesign">
        <div class="card-image  materialboxed">
          <img src="img/port/rick.png">
          <span class="card-title">Web Design</span>
          <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">favorite_border</i></a>
        </div>
        <div class="card-content">
          <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
        </div>
        <div class="card-action">
          <a href="#" class="portfolio-link">This is a link</a>
        </div>
      </div>
    </div>

    <!-- Portfólio 06 -->
    <div class="col s12 m6 l4">
      <div class="card hoverable product-item"  category="webdesign">
        <div class="card-image materialboxed">
          <img  src="img/port/rick.png">
          <span class="card-title">Web Design</span>
          <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">favorite_border</i></a>
        </div>
        <div class="card-content">
          <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
        </div>
        <div class="card-action">
          <a href="#" class="portfolio-link">This is a link</a>

        </div>
      </div>
    </div>

  </div>  <!-- Fecha Div Row Container -->
</div>  <!-- Fecha Div Scroll -->

<!-- Fim Portfólio -->



<!-- === Parallax === -->

<div class="parallax-container parallax-link">
  <div class="parallax"><img src="img/city.png"></div>
  <div class="card-content container center-align txt-parallax-link">
    <h5 class="white-text">I am a very simple card.</h5>
    <p style="color: white">I am a very simple card. I am good at containing small bits of information.<br/></p>
    <br/>
    <a class="waves-effect waves-light btn pulse teal accent-4"><i class="material-icons right">get_app</i>Download</a>
  </div>
</div>

<!-- Fim Parallax -->



<!-- === Sobre === -->

<div id="scroll-sobre">    <!-- Scroll Page Home -->

  <div class="section white">
    <div class="row container">
      <div class="col s12 m3">
      </div>
      <div class="col s12 m12 l6">
        <h4 class="black-text center-align">Sobre</h4>
        <br/><br/>
        <div class="center-align"><img src="img/chimp.png" width="200"></div>

        <div class="card-panel z-depth-0 center-align">
          <i class="material-icons medium section icon-servico">format_quote</i> 
          <span class="black-text">I am a very simple card. I am good at containing small bits of information.
            I am convenient because I require little markup to use effectively. I am similar to what is called a panel in other frameworks.
          </span>          
        </div>
        <div class="col s12 m3">
        </div>
      </div>
    </div>
  </div> <!-- Fecha Div Row -->

</div> <!-- Fecha Div Scroll -->

<!-- Fim Sobre -->



<!-- === Contato === -->
<div id="scroll-contato">   <!-- Scroll Page Contato -->

  <div class="section">
    <h4 class="center-align">Contato</h4>
    <br/>
    <div class="row container">

      <div class="col s12 m12 l6">
        <div class="card-panel hoverable">
          <div class="card-content">
            <div class="center-align"><h5 class="card-title teal-text">Entre em Contato.</h5></div>
          </div>

          <div class="row section">
            <br/>
            <form method="POST" action="" class="col s12">
              <div class="row">
                <div class="input-field col s6">
                  <input placeholder"Placeholder" id="first_name" type="text" class="validate">
                  <label for="first_name">Nome</label>
                </div>
                <div class="input-field col s6">
                  <input id="last_name" type="text" class="validate">
                  <label for="last_name">Sobrenome</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="assunto" type="text" class="validate">
                  <label for="asunto">Assunto</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="email" type="email" class="validate">
                  <label for="email">E-Mail</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <textarea id="textarea1" class="materialize-textarea"></textarea>
                  <label for="textarea1">Mensagem</label>
                </div>
              </div>
              <a class="waves-effect waves-light btn teal accent-4"><i class="material-icons right">send</i>Enviar</a>

            </form>

          </div>

        </div>
      </div>


      <div class="col s12 m12 l6">
       <div class="card teal accent-4">
        <div class="card-content white-text">
          <span class="card-title">Informação de Contato</span>
          <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
        </div>
      </div>

      <ul id="profile-page-about-details" class="collection z-depth-1">
        <li class="collection-item">
          <div class="row">
            <div class="col s5">
              <i class="material-icons left">person</i> Kleyston Marlon</div>
              <div class="col s7 right-align">ABC Name</div>
            </div>
          </li>
          <li class="collection-item">
            <div class="row">
              <div class="col s5">
                <i class="material-icons left">poll</i> Skills</div>
                <div class="col s7 right-align">HTML, CSS</div>
              </div>
            </li>
            <li class="collection-item">
              <div class="row">
                <div class="col s5">
                  <i class="material-icons left">home</i> Lives in</div>
                  <div class="col s7 right-align">NY, USA</div>
                </div>
              </li>
              <li class="collection-item">
                <div class="row">
                  <div class="col s5">
                    <i class="material-icons left">work</i> Birth date</div>
                    <div class="col s7 right-align">18th June, 1991</div>
                  </div>
                </li>
              </ul>              
            </div>

            <!-- Plugin Pág Facebook --> 
            <div class="col s12 m12 l6 left-align">
             <div class="fb-page" data-href="https://www.facebook.com/KleystonDesigner/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/KleystonDesigner/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/KleystonDesigner/">Kleyston Designer</a></blockquote></div>
           </div> 
           <!-- Fim Plugin Pág Facebook --> 
           
         </div>
       </div>

     </div>

     <!-- Fim Contato -->



     <!-- Botão Voltar para Home --> 
     <a href="#scroll-home" class="btn-floating btn-medium waves-effect waves-light scroll teal accent-4 btn-back-home hide-on-med-and-down"><i class="material-icons">keyboard_arrow_up</i></a>



     <!-- === Rodapé === -->

     <footer class="page-footer teal accent-4">
      <div class="container">
        <div class="row">
         <div class="col s12 m6 l3">
           <h5 class="white-text">Footer Content</h5>
           <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
         </div>

         <div class="col s12 m6 l6"> 
         </div>

         <div class="col s12 m6 l3"> 
          <h5 class="white-text">Links</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
          </ul> 
        </div>

      </div>
    </div>

    <div class="footer-copyright center-align">
      <div class="container">
        © 2014 Copyright Sass
      </div>
    </div>
  </footer>

  <!-- Fim Rodapé -->


  <!-- Jquery -->
  <script src="js/jquery-3.2.1.js"></script>

  <!-- Compiled and minified JavaScript -->
  <script src="js/materialize.min.js"></script>

  <!-- JavaScript Filtro  -->
  <script type="text/javascript" src="js/script.js"></script>




  <!-- Inicialização Dropdown -->
  <script>
   $(document).ready(function(){
    $('.parallax').parallax();
  });
</script>   

<!-- Configuração Dropdown -->
<script>
 $('.dropdown-button').dropdown({
  inDuration: 300,
  outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    }
    );
  </script>

  <!-- Animação Velocidade de Rolagem do Scroll One Page --> 
  <script>
   jQuery(document).ready(function($) {
    $(".scroll").click(function(event){
      event.preventDefault();
      $('html,body').animate({scrollTop:$(this.hash).offset().top},1300);
    });
  });
</script>

<!-- Inicialização MaterialBoxed -->
<script>
  $(document).ready(function(){
    $('.materialboxed').materialbox();
  });
</script>

<!-- Opacidade na Barra de Menu -->
<script>
  $(function () {
    "use strict";

    $(window).scroll(function () {
      var $scrollTop = $(this).scrollTop();

      if ($scrollTop < 300) {
        $(".navbar").addClass("navbar-active");
      } else {
       $(".navbar").removeClass("navbar-active");
     }
   });   
  });
</script>



</body>
</html>
